
package lp2inicio;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class Lp2Inicio {

    public static void main(String[] args) {
        String opcao;
        List<Aluno> listaAlunos = new ArrayList<>();
        List<Professor> listaProfessor = new ArrayList<>();

        do {
            opcao = JOptionPane.showInputDialog("1 - para salvar Aluno\n"
                    + "2 - para salvar Professor\n3 - para Sair");
            switch (opcao) {
                case "1":
                    Aluno aluno = new Aluno();
                    aluno.setNome(JOptionPane.showInputDialog("Entre com o nome:"));
                        try 
                        {
                               aluno.setIdade(Integer.parseInt(JOptionPane.showInputDialog("Entre com a Idade: ")));
                        } catch (NumberFormatException erro) 
                        {
                            JOptionPane.showMessageDialog(null, "Caracter inserido incorretamente");
                        } catch (Exception e) 
                        {
                            JOptionPane.showMessageDialog(null, e.getMessage());
                        }
                        aluno.setCpf(Long.parseLong(JOptionPane.showInputDialog("entre com o CPF: ")));
                        aluno.setRa(JOptionPane.showInputDialog("Entre com o RA: "));
                        aluno.setSemestre(Integer.parseInt(JOptionPane.showInputDialog("Entre com o Semestre: ")));
                        listaAlunos.add(aluno);
                    break;
                case "2":
                    //Ler do teclado os valores para professor
                    Professor professor1= new Professor();
                    professor1.setNome(JOptionPane.showInputDialog("Entre com o nome:"));
                        try
                        {
                          professor1.setIdade(Integer.parseInt(JOptionPane.showInputDialog("Entre com a Idade: ")));
                    
                        } 
                       catch (NumberFormatException erro) 
                        {
                            JOptionPane.showMessageDialog(null, "Caracter inserido incorretamente");
                        } catch (Exception e) 
                        {
                            JOptionPane.showMessageDialog(null, e.getMessage());
                        }
                        professor1.setCpf(Long.parseLong(JOptionPane.showInputDialog("entre com o CPF: ")));
                        
                        //Salvar no banco de dados
                        listaProfessor.add(professor1);

                    break;
                case "3":
                    //Sair da aplicação
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Opção Incorreta",
                            "Sistema Escola", 2);
            }
        } while (!opcao.equals("3"));
    }

}
