
<!doctype html>
<html>
	<head>
		<title>Aritmetica</title>
		<meta charset="utf-8">
			
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
			
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.js"></script>
	</head>
	
	<body> 
	 <div class="container"> 
	 
		<form  action="Aritmetica.php" method="get">
			<div>
				<label>Informe um número:  </label> <!--label é um rótulo ou título do que vai ser digitado -->
				<input type="number" name="n1">
			</div>
			<div>
				<label>Informe outro número:  </label> <!--label é um rótulo ou título do que vai ser digitado -->
				<input type="number" name="n2">
			</div>
			<div>
				<label>Informe a operação desejada: </label> 
				<input type="radio" name="operacao" value="x" checked> multiplicação
				<input type="radio" name="operacao" value="+">soma
				<input type="radio" name="operacao" value="-">subtração
				<input type="radio" name="operacao" value="/">divisão
			</div>
			
			
			<input type="submit" value="Calcular ">
			
		</form>
	 
	 </div>
	</body>
</html>
	
	
